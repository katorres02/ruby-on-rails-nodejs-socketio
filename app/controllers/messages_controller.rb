class MessagesController < ApplicationController

  def index
    @messages= Message.all
    @message= Message.new
  end

  def create
    @message = Message.new( message: params[:message][:message])

    respond_to do |format|
      if @message.save
        # format.html { redirect_to @message, notice: t('message.create') }
        # @message['new_count_message']= @message.length.to_s
        # format.json { render :show, status: :created, location: @message}
        format.js
      else
        format.html { render :new }
        format.js
      end
    end
  end

end
