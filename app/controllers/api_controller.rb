class ApiController < ApplicationController
  require 'socket.io-client-simple'

  $socket = SocketIO::Client::Simple.connect('http://localhost:3002')

  def send_message
    Message.create! message: params[:message]
    $socket.emit(:chat_message, params[:message])
    #$socket.disconnect
    render json: 'Cambiado a '+params[:message].to_s, status: :ok
  end

end
