var app     = require('./public/node_modules/express')();
var http    = require('http').Server(app);
var socketio  = require('./public/node_modules/socket.io')();
var port    = 3002;
var io      = socketio.listen( http );



http.listen(port, function(){
  console.log('listening on *: %d', port);
});

socketio.on('connection', function(socket){
  console.log('a user connected ', Date.now());

  socket.on('chat_message', function(msg){
    socketio.emit('chat_message', msg);
    console.log('message del server: ' + msg);
  });

  socket.on('change_color', function(msg){
    socketio.emit('change_color', msg);
    console.log('message del color server: ' + msg);
  });

  socket.on('disconnect', function(){
    console.log('user disconnected ', Date.now());
  });
});
