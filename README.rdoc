== README

Ejemplo de comunicar Ruby On Rails, NodeJS y SocketIO para desarrollar un chat en tiempo real, y comunicación por webservices del chat.
Consumir el webservice /api/send_message?message="Mi mensaje" enviará automáticamente el mensaje al chat visible para todos los clientes.

* rake db:create
* rake db:migrate
* rails server


* En la raiz del proyecto ejecutar " node index.js" que iniciará el servidor nodejs

<tt>rake doc:app</tt>.
